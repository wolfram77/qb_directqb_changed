; ---------------------------------------------------------------------------
;
; directQB VIDEO HANDLING module
;
; Part of the directQB Library version 1.61
; by Angelo Mottola, Enhanced Creations 1998-99
;
; ---------------------------------------------------------------------------

.386

INCLUDE		macros.inc

.STACK 100h

EXTRN	VideoLayers:WORD
EXTRN	VideoLayer:WORD
EXTRN	LastError:WORD

.DATA

.CODE

; ---------------------------------------------------------------------------
; DQBstartVideo SUB
; purpose:
;   Sets VGA 320x200 with 256 colors, and initializes the mouse if found.
; declaration:
;   DECLARE SUB DQBinitVGA
; ---------------------------------------------------------------------------
EVEN
PUBLIC DQBstartVideo
DQBstartVideo PROC
call	DQBmouseHide
mov	ax, 013h
int	010h
call	DQBdisplayVideo
call	DQBmouseShow

ende:
ret
DQBstartVideo ENDP

; ---------------------------------------------------------------------------
; DQBstartText SUB
; purpose:
;   Sets plain 80x25 text mode.
; declaration:
;   DECLARE SUB DQBstartText()
; ---------------------------------------------------------------------------
EVEN
PUBLIC DQBstartText
DQBstartText PROC
mov	ax, 03h
int	010h
ret
DQBstartText ENDP


; ------------------------------------------------------------------------------------------------------------------------------------------------------
; DQBuseVideoLayer SUB
; purpose:
;   Map a layer as Video Layer
; declaration:
;   DEclARE SUB DQBuseVideoLayer(BYVAL Layer%) 
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC DQBuseVideoLayer
DQBuseVideoLayer PROC FAR
UseParam
mov	bx, param1
cmp	bx, VideoLayers
ja	layererr

layeravail:
mov	VideoLayer, bx
call	GetEMSseg
mov	b$segC, ax

layerover:
EndParam
ret

layererr:
mov	LastError, errnotvideolayer
jmp	layerover
DQBuseVideoLayer ENDP


; ---------------------------------------------------------------------------
; DQBcopyLayer SUB
; purpose:
;   Copies a layer onto another one.
; declaration:
;   DECLARE SUB DQBcopyLayer(BYVal DestLayer%, BYVal SourceLayer%)
; ---------------------------------------------------------------------------
EVEN
PUBLIC DQBcopyLayer
DQBcopyLayer PROC
UseParam
mov	ax, VideoLayers
cmp	ax, param1
ja	verr
cmp	ax, param2
ja	verr
push2	si, di
mov	fs, param1
mov	es, param2
Null	si
Null	di
mov	ax, 101h
mov	cx, videosize
call	CopyMem
pop2	si, di

vover:
EndParam
ret	4

verr:
mov	LastError, errnotvideolayer
jmp	vover
DQBcopyLayer ENDP

; ---------------------------------------------------------------------------
; DQBclearLayer SUB
; purpose:
;   Clears the content of a given layer to black (color 0 used)
; declaration:
;   DECLARE SUB DQBclearLayer(BYVal Layer%)
; ---------------------------------------------------------------------------
EVEN
PUBLIC DQBclearLayer
DQBclearLayer PROC
UseParam
push	di
mov	ax, param1
cmp	ax, VideoLayers
ja	verr
call	GetEMSseg
mov	es, ax
mov	cx,16000
xor	eax, eax
xor	di,di
rep	stosd

vover:
pop	di
EndParam
ret	2

verr:
mov	LastError, errnotvideolayer
jmp	vover
DQBclearLayer ENDP

; ---------------------------------------------------------------------------
; DQBdisplayWait SUB
; purpose:
;   Waits for video vertical retrace
; declaration:
;   DECLARE SUB DQBdisplayWait( )
; ---------------------------------------------------------------------------
EVEN
PUBLIC DQBdisplayWait
DQBdisplayWait PROC
push2	dx, ax
mov	dx,3DAh       ; Vertical retrace port

Vretover:
in	al,dx
and	al,8
jnz	Vretover

Vretstart:
in	al,dx
and	al,8
jz	Vretstart
pop2	dx, ax
ret
DQBdisplayWait ENDP


; ---------------------------------------------------------------------------
; DQBdisplayVideo SUB
; purpose:
;   Waits for video vertical retrace and then displays the Videolayer on screen
; declaration:
;   DECLARE SUB DQBdisplayVideo( )
; ---------------------------------------------------------------------------
EVEN
PUBLIC DQBdisplayVideo
DQBdisplayVideo PROC
mov	ax, VideoLayer
call	GetEMSseg
mov	fs, ax
Null	si
mov	es, VideoSegment
Null	di
mov	cx, 64000
call	DQBdisplayWait
call	CopyBytes
call	DrawMouse
ret
DQBdisplayVideo ENDP
