; ---------------------------------------------------------------------------
;
; directQB STRING HANDLING module
;
; Part of the directQB Library version 1.61
; by Angelo Mottola, Enhanced Creations 1998-99
;
; ---------------------------------------------------------------------------

.386

INCLUDE		macros.inc

.STACK 100h

EXTRN	EMScopy		EMScopyVar <>
EXTRN	EMSpage:WORD
EXTRN	EMShandle:WORD
EXTRN	EMSseg:WORD
EXTRN	VideoLayers:WORD
EXTRN	VideoLayer:WORD
EXTRN	GeneralStringSize:WORD
EXTRN	LastError:WORD

.DATA

.CODE



; ---------------------------------------------------------------------------
; CompareStrings INTERNAL FUNCTION
; purpose:
;   Checks two LASCIIZ strings for equality(only in CONV Mem)
; usage:
;   fs:si=string1, es:di=string2
;  destroys:
;   cx, ax=-1 or 0(e/ne)
; ---------------------------------------------------------------------------
EVEN
PUblIC CompareStrings
CompareStrings PROC
push4	si, di, bx, cx
mov	cx, fs:[si]
cmp	cx, es:[di]
jne	strne
mov	bx, cx
shr	cx, 2
or	cx, cx
jz	stre

loopdword:
add	si, 4
add	di, 4
mov	eax, fs:[si]
cmp	eax, es:[di]
jne	strne
dec	cx
jnz	loopdword
mov	cx, bx
and	cx, 3

loopbyte:
inc	si
inc	di
mov	al, fs:[si]
cmp	al, es:[di]
jne	strne
dec	cx
jnz	loopbyte

stre:
mov	ax, -1		;equal

strover:
pop4	si, di, bx, cx
ret

strne:
Null	ax
jmp	strover
CompareStrings ENDP



END