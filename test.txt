; ---------------------------------------------------------------------------
; ReadDatumFile Internal Function
; purpose:
;   Reads an datum from file(Windows one if possible)
; usage:
;   bx=filenum, dx=bytes
;  destroys:
;  eax=data
; ---------------------------------------------------------------------------
EVEN
PUblIC ReadDatumFile		;repair
ReadDatumFile PROC
push	cx
mov	cx, bx
FileHandleCheckActive	bx, fileriover
cmp	cx, ReadDatumFileNum
jne	fileriextract
mov	ax, ReadDatumLast
cmp	ax, ReadDatumLength
jae	filerihowread

filerinormread:
add	ax, dx
mov	ReadDatumLast, ax
mov	ax, ReadDatumBuffer
cmp	ax, EMSseg
jne	set

filerinorm:
mov	fs, ax
mov	bx, ReadDatumBuffer[2]
add	bx, ReadDatumLast
mov	eax, fs:[bx]

fileriover:
pop	cx
ret

set:
call	GetEMSseg
jmp	filerinorm

filerihowread:
cmp	ax, ReadDatumSize
jb	fileriextract
mov	LastError, errfileover
jmp	fileriover

fileriextract:
mov	ReadDatumFileNum, cx
mov	ax, ReadDatumBuffer
call	GetEMSseg
ReadFile	ax, ReadDatumBuffer[2], ReadDatumSize, bx
mov	ReadDatumLength, ax
jnc	fileriok1
mov	LastError, errfileuse
mov	DOSerror, ax
jmp	fileriover

fileriok1:
Nullx	ReadDatumLast
jmp	filerinormread
ReadDatumFile ENDP


; ---------------------------------------------------------------------------
; WriteDatumFile Internal Function
; purpose:
;   Writes an datum from file(Windows one if possible)
; usage:
;   bx=filenum, dx=bytesadd, eax=data
;  destroys:
;  bx, dx
; ---------------------------------------------------------------------------
EVEN
PUblIC WriteDatumFile
WriteDatumFile PROC
push	cx
mov	FreeVarD, eax
mov	cx, bx
FileHandleCheckActive	bx, fileriover
cmp	cx, WriteDatumFileNum
jne	filerisaveprev
mov	ax, WriteDatumLast
cmp	ax, WriteDatumSize
jae	filerisaveprev

filerinormwrite:
add	ax, dx
mov	WriteDatumLast, ax
mov	ax, WriteDatumBuffer
cmp	ax, EMSseg
jne	set

filerinorm:
mov	fs, ax
mov	bx, WriteDatumBuffer[2]
add	bx, WriteDatumLast
mov	eax, FreeVarD
mov	fs:[bx], eax

fileriover:
pop	cx
ret

set:
call	GetEMSseg
jmp	filerinorm


filerisaveprev:
mov	WriteDatumFileNum, cx
push	bx
mov	bx, WriteDatumBuffer
call	GetEMSseg
pop	bx
WriteFile	cx, WriteDatumBuffer[2], WriteDatumLast, bx
jnc	fileriok1
mov	LastError, errfileuse
mov	DOSerror, ax
jmp	fileriover

fileriok1:
Nullx	WriteDatumLast
jmp	filerinormwrite
WriteDatumFile ENDP

