

;INCLUDE		main.asm

; ***************************************************************************
; Macros
; ***************************************************************************
Null		MACRO	var
xor	var, var
ENDM

Nullx		MACRO	var
mov	var, 0
ENDM

UseParam		MACRO
push	bp
mov	bp, sp
ENDM

EndParam		MACRO
pop	bp
ENDM

movx			MACRO	dest, src, dummy
mov	dummy, src
mov	dest, dummy
ENDM

cmpx			MACRO	dest, src, dummy
mov	dummy, src
cmp	dest, dummy
ENDM

addx			MACRO	dest, src, dummy
mov	dummy, src
add	dest, dummy
ENDM

xchgx			MACRO	dest, src, dummy
mov	dummy, src
xchg	dest, dummy
mov	src, dummy
ENDM

pushx			MACRO	src, dummy
mov	dummy, src
push	dummy
ENDM


LookToData		MACRO
mov	ax, @DATA
mov	ds, ax
ENDM

EMSmapPage		MACRO
mov	ah, 44h
int	67h
ENDM

WriteFile			MACRO	DataSeg , DataOff, Length, Handle
push	ds
mov	ax, DataSeg
mov	ds, ax
mov	ah, 40h
mov	bx, Handle
mov	cx, Length
mov	dx, DataOff
int	21h
pop	ds
ENDM

ReadFile			MACRO	DataSeg, DataOff, Length, Handle
push	ds
mov	ax, DataSeg
mov	ds, ax
mov	ah, 3Fh
mov	bx, Handle
mov	cx, Length
mov	dx, DataOff
int	21h
pop	ds
ENDM

;Mode:0=start,1=current,2=end:DX:AX=new file pos
SeekFile			MACRO	PositionHI, PositionLO, Mode, Handle
mov	ah, 42h
mov	al, Mode
mov	bx, Handle
mov	cx, PositionHI
mov	dx, PositionLO
int	21h
ENDM

;Mode=access/sharing mode
;0-2=R,W,RW,?:3=0:4-6=comp,lockRW,lockW,lockR,nolock
;name=ASCIIZ
OpenFile			MACRO	FileNameSeg, FileNameOff, Mode, Handle
push	ds
mov	ax, FileNameSeg
mov	ds, ax
mov	ah, 3Dh
mov	al, Mode
mov	dx, FileNameOff
xor	cl, cl
int	21h
mov	Handle, ax
pop	ds
ENDM

CloseFile			MACRO	Handle
mov	ah, 3Eh
mov	bx, Handle
ENDM

;Mode=(bitwise)readonly,hidden,system,?,0,archive,?
CreateFile		MACRO	FileNameSeg, FileNameOff, Mode, Handle
puh	ds
mov	ax, FileNameSeg
mov	ds, ax
mov	ah, 3Ch
mov	cx, Mode
mov	dx, FileNameOff
int	21h
mov	Handle, ax
pop	ds
ENDM

GetInterrupt		MACRO	Interrupt, IntSeg, IntOff
push	ds
mov	ah, 35h
mov	al, Interrupt
int	21h
mov	ax, @DATA
mov	ds, ax
mov	IntSeg, es
mov	IntOff, bx
pop	ds
ENDM

SetInterrupt		MACRO	Interrupt, IntSeg, IntOff
push	ds
mov	ax, @DATA
mov	ds, ax
mov	ax, IntSeg
mov	dx, IntOff
mov	ds, ax
mov	ah, 25h
mov	al, Interrupt
int	21h
pop	ds
ENDM


FileHandleCheckInactive		MACRO	par, fileover
mov	bx, par
cmp	bx, 255
jbe	filenumnotbig
mov	LastError, errfilenumber
jmp	fileover

filenumnotbig:
mov	bx, FileHandle[2*bx]
cmp	bx, 0
je	filehandleok
mov	LastError, errfilenumber
jmp	fileover

filehandleok:
ENDM



FileHandleCheckActive		MACRO	par, fileover
mov	bx, par
cmp	bx, 255
jbe	filenumnotbig
mov	LastError, errfilenumber
jmp	fileover

filenumnotbig:
mov	bx, FileHandle[2*bx]
cmp	bx, 0
jne	filehandleok
mov	LastError, errfilenumber
jmp	fileover

filehandleok:
ENDM




;*****Structures*****
EMScopyVar STRUC
len1		DW	?
len2		DW	?
sourceType	DB	?
sourceHdl	DW	?
sourceOff		DW	?
sourceSegPg	DW	?
destType		DB	?
destHdl		DW	?
destOff		DW	?
destSegPg	DW	?
EMScopyVar ENDS



;*****Constants*****
param1		equ	[bp+6]
param2		equ	[bp+8]
param3		equ	[bp+10]
param4		equ	[bp+12]
param5		equ	[bp+14]
param6		equ	[bp+16]
param7		equ	[bp+18]
param8		equ	[bp+20]
param9		equ	[bp+22]
param10		equ	[bp+24]
param11		equ	[bp+26]
param12		equ	[bp+28]
stack0		equ	[bp]
stack1		equ	[bp+01]
stack2		equ	[bp+02]
stack3		equ	[bp+03]
stack4		equ	[bp+04]
stack5		equ	[bp+05]
stack6		equ	[bp+06]
stack7		equ	[bp+07]
stack8		equ	[bp+08]
stack9		equ	[bp+09]
stack10		equ	[bp+10]
stack11		equ	[bp+11]
stack12		equ	[bp+12]
stack13		equ	[bp+13]
stack14		equ	[bp+14]
stack15		equ	[bp+15]
stack16		equ	[bp+16]
stack17		equ	[bp+17]
stack18		equ	[bp+18]
stack19		equ	[bp+19]
stack20		equ	[bp+20]
videolayer		equ	0
joyport		equ	201h
keyport		equ	60h
keytellport		equ	61h
irqport		equ	20h
keyshiftflag	equ	417h
joy1x		equ	0
joy1y		equ	1
joy1a		equ	2
joy1b		equ	3
joy2x		equ	4
joy2y		equ	5
joy2a		equ	6
joy2b		equ	7
joy1		equ	0
joy2		equ	1


;*****Errors*****

