; ---------------------------------------------------------------------------
;
; DirectQB GSL File SUPPORT module
;
; Part of the DirectQB Library version 1.61
; by Angelo Mottola, Enhanced Creations 1998-99
;
; ---------------------------------------------------------------------------

.MODEL medium,basic

.386

.STACK 100h

;Byte 24: Number of Data Packets
;Byte 28: Offset to name list
;Byte 32: Offset to type list
;Byte 36: Offset to number list


EXTRN	DataPacketGsl:BYTE		;28 bytes
;0 FileNum
;2 FileHandle
;4 DataPacketNum
;Byte x+8: Offset to Name
;Byte x+12: Data Packet Type
;Byte x+16: Offset to Data Packet Type name
;Byte x+20: Offset to Data Packet
;Byte x+24: Data Packet size


.CODE

; ------------------------------------------------------------------------------------------------------------------------------------------------------
; ChecksumGsl INTERNAL FUNCTION
; purpose:
;   Check if file checksum is correct
; usage:
;   dx=filehandle
; destoys:
;   none
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC ChecksumGsl
ChecksumGsl PROC FAR
push2	eax, ebx
mov	ebx, gslchecksum
mov	bx, dx
call	FileSum
mov	ebx, eax
mov	eax, gslchecksum
call	ReadDatumFile
cmp	eax, ebx
jne	chkerr
clc

chkok:
pop2	eax, ebx
ret

chkerr:
mov	LastError, errgslchecksum
stc
jmp	chkok
ChecksumGsl ENDP

; ---------------------------------------------------------------------------
; OpenGsl INTERNAL FUNCTION
; purpose:
;   Opens a specified GSL file(Windows one if possible)
; usage:
;   fs:si=File Name, ax=filenum, bl=mode, bh=check file integrity(0/1)
;  destroys:
;  ax=file handle, fs, si, ax, bx
; ---------------------------------------------------------------------------
EVEN
PUBLIC OpenGsl
OpenGsl PROC FAR
push6	es, di, dx, cx, dx, bx
call	OpenFile
pop	bx
test	bh, 1
jz	nocheck
call	ChecksumGsl
jc	openover

nocheck:
mov	es, GslFileTable
pop	di
shl	di, 4
add	di, GslFileTable[2]
mov	al, 1
mov	ebx, gslfiletable+4
mov	cx, 16
call	CopyDataToMemFromFile

openover:
pop4	es, di, dx, cx
ret
OpenGsl ENDP

; ---------------------------------------------------------------------------
; CheckGsl INTERNAL FUNCTION
; purpose:
;   Sees if GSL file opened, also sets es:di to file table(Windows one if possible)
; usage:
;   dx=filenum
;  destroys:
;  ax=file handle, es:di=file table
; ---------------------------------------------------------------------------
EVEN
PUBLIC CheckGsl
CheckGsl PROC FAR
clc
push2	es, di
mov	di, dx
mov	ax, FileHandle[2*di]
or	ax, ax
jz	chkerr
mov	es, GslFileTable
shl	di, 4
add	di, GslFileTable[2]
cmp	es:[di+12], 0
jz	chkerr

chkover:
pop2	es, di
ret

chkerr:
mov	LastError, errgslnotopened
stc
jmp	chkover
CheckGsl ENDP


; ---------------------------------------------------------------------------
; GetDataPacketNumGsl INTERNAL FUNCTION
; purpose:
;   Gets the info, seek address of a data packet from GSL file(Windows one if possible)
; usage:
;   dx=filenum, ebx=data packet num
;  destroys:
;  ax=file handle, bx
; ---------------------------------------------------------------------------
EVEN
PUBLIC GetDataPacketNumGsl
GetDataPacketNumGsl PROC FAR
clc
push4	es, di, cx, dx
mov	WORD PTR DataPacketGsl, -1
call	CheckGsl
jc	getover
cmp	es:[di], ebx
ja	geterrnotenoughdata
mov	DataPacketGsl, dx
mov	DataPacketGsl[2], ax
mov	DataPacketGsl[4], ebx
mov	dx, ax
mov	eax, ebx
shl	eax, 2
shl	ebx, 4
add	ebx, eax
add	ebx, es:[di+12]
Null	al
movx	es, ds, cx
mov	di, OFFSET DataPacketGsl[8]
mov	cx, 20
call	CopyDataToMemFromFile
mov	ax, dx

getover:
pop4	es, di, cx, dx
ret

geterrnotenoughdata:
mov	LastError, errgslnosuchdatapacket
stc
jmp	getover
GetDataPacketNumGsl ENDP

; ---------------------------------------------------------------------------
; GetDataGsl INTERNAL FUNCTION
; purpose:
;   Gets the data after data packet info has been got from GSL file(Windows one if possible)
; usage:
;   al=mem type, es:di=dest
;  destroys:
;  es, ax
; ---------------------------------------------------------------------------
EVEN
PUBLIC GetDataGsl
GetDataGsl PROC FAR
clc
push3	ebx, cx, dx
mov	ebx, DataPacketGsl[20]
or	ebx, ebx
jz	geterrdatnotgot
mov	dx, DataPacketGsl[2]
mov	cx, DataPacketGsl[24]
call	CopyDataToMemFromFile

getover:
pop3	ebx, cx, dx
ret

geterrdatnotgot:
mov	LastError, errgsldatanotgot
stc
jmp	getover

; ---------------------------------------------------------------------------
; GetDataPacketNameGsl INTERNAL FUNCTION
; purpose:
;   Gets the info, seek address of a data packet from GSL file(Windows one if possible)
; usage:
;   dx=filenum, fs:si=datapacketLASCIZname
;  destroys:
;  ax=file handle, bx
; ---------------------------------------------------------------------------
EVEN
PUBLIC GetDataPacketNumGsl
GetDataPacketNumGsl PROC FAR
clc
push4	es, di, cx, dx
mov	WORD PTR DataPacketGsl, -1
call	CheckGsl
jc	getover
mov	ebx, es:[di+4]
mov	dx, FileHandle
cmp	es:[di], ebx
ja	geterrnotenoughdata
mov	DataPacketGsl, dx
mov	DataPacketGsl[2], ax
mov	DataPacketGsl[4], ebx
mov	dx, ax
mov	eax, ebx
shl	eax, 2
shl	ebx, 4
add	ebx, eax
add	ebx, es:[di+12]
Null	al
movx	es, ds, cx
mov	di, OFFSET DataPacketGsl[8]
mov	cx, 20
call	CopyDataToMemFromFile
mov	ax, dx

getover:
pop4	es, di, cx, dx
ret

geterrnotenoughdata:
mov	LastError, errgslnosuchdatapacket
stc
jmp	getover
GetDataPacketNumGsl ENDP














































END