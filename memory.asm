; ---------------------------------------------------------------------------
;
; directQB MEMORY HANDLING module
;
; Part of the directQB Library version 1.61
; by Angelo Mottola, Enhanced Creations 1998-99
;
; ---------------------------------------------------------------------------

.386

INCLUDE		macros.inc

.STACK 100h

EXTRN	EMScopy		EMScopyVar <>
EXTRN	EMSpage:WORD
EXTRN	EMShandle:WORD
EXTRN	EMSseg:WORD
EXTRN	GeneralStringSize:WORD
EXTRN	LastError:WORD

.DATA

.CODE

; ------------------------------------------------------------------------------------------------------------------------------------------------------
;GetEMSseg internal function
; purpose:
;   Maps the 4 physical pages(1 seg), and returns EMS segment(0=video)
; calling:
;   ax  Logical page to be mapped(64K)(ds to look to data)
; changes:
;   ax=EMS segment
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC GetEMSseg
GetEMSseg PROC FAR
cmp	ax, EMSpage
je	PreMapped
cmp	ax, videolayer
je	videos
mov	EMSpage, ax
push	dx
mov	dx, EMShandle
xor	al, al 

MapPage:
EMSmapPage
inc	al
cmp	al, 4
jne	MapPage
pop	dx
mov	ax, EMSseg

PreMapped:
ret

videos:
mov	ax, videoseg
ret
GetEMSseg ENDP


; ------------------------------------------------------------------------------------------------------------------------------------------------------
; CopyBytes internal function
; purpose:
;  Copy from conventional memory
; calling:
;   fs:si=source, es:di=dest, cx=bytes
; changes:
;    cx
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC CopyBytes
CopyBytes PROC FAR
push4	ds, bx, si, di
mov	ds, fs, bx
mov	bx, cx
shr	cx, 2
rep	movsd
mov	cx, bx
and	cx, 3
rep	movsb
pop4	ds, bx, si, di
ret
CopyBytes ENDP

; ------------------------------------------------------------------------------------------------------------------------------------------------------
; GetMem internal function
; purpose:
;  Get CONV address for  conventional/EMS memory
; calling:
;   es:di=memory, al= memtype (src) ---->es:di=CONV address
; changes:
;   es=CONV address
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC GetMem
GetMem PROC FAR
push	ax
test	al, 1
jz	alreadyconv
mov	ax, es
call	GetEMSseg
mov	es, ax

alreadyconv:
pop	ax
ret
GetMem ENDP

; ------------------------------------------------------------------------------------------------------------------------------------------------------
; CopyMem internal function
; purpose:
;  Copy from conventional/EMS memory
; calling:
;   fs:si=source, es:di=dest, al,ah= memtype (src,dest),cx=bytes
; changes:
;   ax, cx, fs, es
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC CopyMem
CopyMem PROC FAR
push	dx
cmp	ax, 101h
je	copymemuseems
call	GetMem
xchgx	fs, es, dx
call	GetMem
xchgx	fs, es, dx
call	CopyBytes

copymemdr:
pop	dx
ret

copymemuseems:
mov	EMScopy.len1, dx		;dont use this. use ems physical pages
mov	EMScopy.len2, 0
mov	EMScopy.sourceType, al
mov	EMScopy.destType, ah
mov	ax, EMShandle
mov	EMScopy.sourceHdl, ax
mov	EMScopy.destHdl, ax
mov	ax, fs
dec	ax
shl	ax, 2
mov	EMScopy.sourceSegPg
mov	ax, es
dec	ax
shl	ax, 2
mov	EMScopy.destSegPg
mov	EMScopy.sourceOff, si
mov	EMScopy.destOff, di
mov	si, OFFSET EMScopy
mov	ax, 5700h
int	67h
jmp	copymemdr
CopyMem ENDP

; ------------------------------------------------------------------------------------------------------------------------------------------------------
; GetStringMem internal function
; purpose:
;  Get LASCIZ string from conventional/EMS memory
; calling:
;   fs:si=source, es:di=dest, al= memtype (src)
; changes:
;   ax, fs, es
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC GetStringMem
GetStringMem PROC FAR
push2	cx, si
xchgx	fs, es, cx
call	GetMem
xchgx	fs, es, cx
mov	cx, fs:[si]
add	si, 2
cmp	cx, GeneralStringSize
ja	strszerr

strszok:
inc	cx
mov	ah, 0
call	CopyMem

getstrover:
pop2	cx, si
ret

strszerr:
mov	LastError, errlargestring
mov	byte ptr es:[di], 0
jmp	getstrover
GetStringMem ENDP

; ------------------------------------------------------------------------------------------------------------------------------------------------------
; WriteStringMem internal function
; purpose:
;  Write LASCIZ string to conventional/EMS memory
; calling:
;   fs:si=source, es:di=dest, al= memtype (dest), cx=length
; changes:
;   ax, fs, es, cx
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC GetStringMem
GetStringMem PROC FAR
cmp	cx, GeneralStringSize
ja	strszerr
push	di
call	GetMem
mov	es:[di], cx
add	di, 2

strszok:
inc	cx
mov	ah, 0
call	CopyMem
pop	di

getstrover:
ret

strszerr:
mov	LastError, errlargestring
mov	word ptr es:[di], 0
mov	byte ptr es:[di+2], 0
jmp	getstrover
GetStringMem ENDP


; ------------------------------------------------------------------------------------------------------------------------------------------------------
; PokeMem internal function
; purpose:
; purpose:
;  Poke data to memory
; calling:
;   es:di=mem, bl= memtype (src) bh=bytes dx:ax=data
; changes:
;   ax, bx, dx
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC PokeMem
PokeMem PROC FAR
UseParam
call	GetMem
cmp	bh, 1
ja	pokew
mov	es:[di], al
ret

pokew:
cmp	bh, 2
mov	es:[di], ax
ja	poked
ret

poked:
mov	es:[di+2], dx
ret
PokeMem ENDP

; ------------------------------------------------------------------------------------------------------------------------------------------------------
; PeekMem internal function
; purpose:
; purpose:
;  Peek data from memory
; calling:
;   es:di=mem, bl= memtype (src) bh=bytes ----> dx:ax=data
; changes:
;   ax, bx, cx, dx
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC PeekMem
PeekMem PROC FAR
UseParam
call	GetMem
cmp	bh, 1
ja	peekw
mov	al, es:[di]
ret

peekw:
cmp	bh, 2
mov	ax, es:[di]
ja	peekd
ret

peekd:
mov	dx, es:[di]
ret
PeekMem ENDP

; ------------------------------------------------------------------------------------------------------------------------------------------------------
; ReplaceDataMem internal function
; purpose:
;  Replace a piece of data in memory
; calling:
;   fs:si=Mem to  be replaced, es:di=Mem to replace, bx=Replaced full size, cx=be replaced size, dx=replacer size, (al,ah)=memtypes(1,2)
; changes:
;   ax, bx, cx, dx, si, di, fs, es
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC ReplaceDataMem
ReplaceDataMem PROC FAR
cmp	bx, cx
jb	repshftdn
ja	repshftdup
call	CopyMem
ret

repshftdn:
mov	FreeVar, si
pushf
push6	fs, si, es, di, dx, ax
add	si, bx
sub	si, 4
cmp	si, FreeVar
jae	look
mov	si, FreeVar

look:
dec	si
std

repshftcommon:
movx	es, fs, di
mov	di, si
add	di, dx
sub	di, cx
mov	ah, al
mov	cx, bx
call	CopyMem
cld
pop2	cx, ax
pop4	es, di, fs, si
call	CopyMem
popf
ret

repshftup:
pushf
push6	fs, si, es, di, dx, ax
cld
jmp	repshftcommon
ReplaceDataMem ENDP


; ------------------------------------------------------------------------------------------------------------------------------------------------------
; DQBpoke SUB
; purpose:
;   Write a byte to memory
; declaration:
;   DEclARE SUB DQBpoke(BYVAL MemType%, BYVAL MemSeg%, BYVAL MemOff%, BYVAL Data%) 
;   DEclARE SUB DQBpoke(BYVAL MemType%, BYVAL MemAddr&, BYVAL Data%) 
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC DQBpoke
DQBpoke PROC FAR
UseParam
mov	bl, param4
mov	bh, 1
mov	es, param3
mov	di, param2
mov	ax, param1
call	PokeMem
EndParam
ret
DQBpoke ENDP

; ------------------------------------------------------------------------------------------------------------------------------------------------------
; DQBpokeInteger SUB
; purpose:
;   Write an integer(word) to memory
; declaration:
;   DEclARE SUB DQBpokeInteger(BYVAL MemType%, BYVAL MemSeg%, BYVAL MemOff%, BYVAL Data%) 
;   DEclARE SUB DQBpokeInteger(BYVAL MemType%, BYVAL MemAddr&, BYVAL Data%) 
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC DQBpokeInteger
DQBpokeInteger PROC FAR
UseParam
mov	bl, param4
mov	bh, 2
mov	es, param3
mov	di, param2
mov	ax, param1
call	PokeMem
EndParam
ret
DQBpokeInteger ENDP

; ------------------------------------------------------------------------------------------------------------------------------------------------------
; DQBpokeLong SUB
; purpose:
;   Write an long(dword) to memory
; declaration:
;   DEclARE SUB DQBpokeLong(BYVAL MemType%, BYVAL MemSeg%, BYVAL MemOff%, BYVAL Data1%, BYVAL Data2%) 
;   DEclARE SUB DQBpokeLong(BYVAL MemType%, BYVAL MemAddr&, BYVAL Data&) 
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC DQBpokeLong
DQBpokeLong PROC FAR
UseParam
mov	bl, param5
mov	bh, 4
mov	es, param4
mov	di, param3
mov	dx, param2
mov	ax, param1
call	PokeMem
EndParam
ret
DQBpokeLong ENDP


; ------------------------------------------------------------------------------------------------------------------------------------------------------
; DQBpeek FUNCTION
; purpose:
;   Read a byte from memory
; declaration:
;   DEclARE FUNCTION DQBpeek(BYVAL MemType%, BYVAL MemSeg%, BYVAL MemOff%) 
;   DEclARE FUNCTION DQBpeek(BYVAL MemType%, BYVAL MemAddr&) 
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC DQBpeek
DQBpeek PROC FAR
UseParam
mov	bl, param3
mov	bh, 1
mov	es, param2
mov	di, param1
call	PeekMem
EndParam
ret
DQBpeek ENDP

; ------------------------------------------------------------------------------------------------------------------------------------------------------
; DQBpeekInteger FUNCTION
; purpose:
;   Read an integer(word) from memory
; declaration:
;   DEclARE FUNCTION DQBpeekInteger(BYVAL MemType%, BYVAL MemSeg%, BYVAL MemOff%) 
;   DEclARE FUNCTION DQBpeekInteger(BYVAL MemType%, BYVAL MemAddr&) 
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC DQBpeekInteger
DQBpeekInteger PROC FAR
UseParam
mov	bl, param4
mov	bh, 2
mov	es, param3
mov	di, param2
call	PeekMem
EndParam
ret
DQBpeekInteger ENDP

; ------------------------------------------------------------------------------------------------------------------------------------------------------
; DQBpeekLong FUNCTION
; purpose:
;   Read an long(dword) from memory
; declaration:
;   DEclARE FUNCTION DQBpeekLong(BYVAL MemType%, BYVAL MemSeg%, BYVAL MemOff%) 
;   DEclARE FUNCTION DQBpeekLong(BYVAL MemType%, BYVAL MemAddr&) 
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC DQBpeekLong
DQBpeekLong PROC FAR
UseParam
mov	bl, param5
mov	bh, 4
mov	es, param4
mov	di, param3
call	PokeMem
EndParam
ret
DQBpeekLong ENDP


; ------------------------------------------------------------------------------------------------------------------------------------------------------
; DQBcopyMem SUB
; purpose:
;   Copy from conventional/EMS memory
; declaration:
;   DEclARE SUB DQBcopyMemory(BYVAL DestMemType%, BYVAL DestSeg%, BYVAL DestOff%, 
; 			BYVAL SrcMemType%, BYVAL SrcSeg%, BYVAL SrcOff%, BYVAL Length%)
;   DEclARE SUB DQBcopyMemory(BYVAL DestMemType%, BYVAL Destination&, BYVAL SrcMemType%, BYVAL Source&, BYVAL Length%)
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC DQBCopyMemory
DQBCopyMemory PROC FAR
UseParam
push2	si, di
mov	ah, param7
mov	es, param6
mov	di, param5
mov	al, param4
mov	fs, param3
mov	si, param2
mov	cx, param1
call	CopyMem
pop2	si, di
EndParam
DQBCopyMemory ENDP

; ------------------------------------------------------------------------------------------------------------------------------------------------------
; DQBgetStringFromMem FUNCTION
; purpose:
;   Get LASCIZ string from conventional/EMS memory
; declaration:
;   DEclARE FUNCTION xDQBgetStringFromMemory(BYVAL StrSeg%, BYVAL StrOff%, BYVAL MemType%, BYVAL MemSeg%, BYVAL MemOff%)
;   DEclARE FUNCTION DQBgetStringFromMemory$(BYVAL MemType%, BYVALMemoryAddr&)
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC  xDQBgetStringFromMemory
xDQBgetStringFromMemory PROC FAR
UseParam
push2	fs,si, di
mov	es, param5
mov	di, param4
mov	ax, param3
mov	fs, param2
mov	si, param1
call	GetStringMem
pop2	fs, si, di
EndParam
xDQBgetStringFromMemory ENDP


; ------------------------------------------------------------------------------------------------------------------------------------------------------
; DQBReplaceDataInMem SUB
; purpose:
;   Replace a piece of data in memory
; declaration:
;   DEclARE SUB DQBReplaceDataInMemory(BYVAL ToBeReplacedType%, BYVAL ToBeReplacedHI%, BYVAL ToBeReplacedLO%, BYVAL ToBeReplacedSize%,
;			BYVAL  BeReplacedSize%,  BYVAL ToReplaceType%, BYVAL ToReplaceHI%, BYVAL ToReplaceLO%, BYVAL ReplacerSize%)
;   DEclARE SUB DQBReplaceDataInMemory(BYVAL ToBeReplacedType%, BYVAL ToBeReplaced&, BYVAL ToBeReplacedSize%, BYVAL  BeReplacedSize%,
;			BYVAL ToReplaceType%, BYVAL ToReplace&, BYVAL ReplacerSize%)
; ------------------------------------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC  DQBReplaceDataInMemory
DQBReplaceDataInMemory PROC FAR
UseParam
push2	fs, si, di
mov	al, param9
mov	fs, param8
mov	si, param7
mov	bx, param6
mov	cx, param5
mov	ah, param4
mov	es, param3
mov	di, param2
mov	dx, param1
call	ReplaceDataMem
pop2	fs, si, di
EndParam
ret
DQBReplaceDataInMemory ENDP


END
